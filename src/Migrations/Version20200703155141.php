<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200703155141 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE account (id INT AUTO_INCREMENT NOT NULL, firstname VARCHAR(255) DEFAULT NULL, lastname VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, adress VARCHAR(255) DEFAULT NULL, phonenumber VARCHAR(255) DEFAULT NULL, isactive TINYINT(1) DEFAULT NULL, password VARCHAR(255) DEFAULT NULL, role VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, kind VARCHAR(255) NOT NULL, score INT DEFAULT NULL, socialreason VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE paymentmethod (id INT AUTO_INCREMENT NOT NULL, account_id INT DEFAULT NULL, kind VARCHAR(255) NOT NULL, bankname VARCHAR(255) DEFAULT NULL, accounttype VARCHAR(255) DEFAULT NULL, cardnumber VARCHAR(255) DEFAULT NULL, cardname VARCHAR(255) DEFAULT NULL, expirationdate DATETIME DEFAULT NULL, cryptogram INT DEFAULT NULL, phonenumber VARCHAR(255) DEFAULT NULL, serviceprovider VARCHAR(255) DEFAULT NULL, currentbalance DOUBLE PRECISION DEFAULT NULL, INDEX IDX_8328E04E9B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE claim (id INT AUTO_INCREMENT NOT NULL, personal_id INT DEFAULT NULL, state VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, object VARCHAR(255) DEFAULT NULL, processingdate DATETIME DEFAULT NULL, INDEX IDX_A769DE275D430949 (personal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document (id INT AUTO_INCREMENT NOT NULL, account_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, path VARCHAR(255) DEFAULT NULL, typedocument VARCHAR(255) DEFAULT NULL, INDEX IDX_D8698A769B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE installmentpayment (id INT AUTO_INCREMENT NOT NULL, request_payment_id INT DEFAULT NULL, refunddate DATETIME DEFAULT NULL, balance DOUBLE PRECISION DEFAULT NULL, ispayed TINYINT(1) DEFAULT NULL, INDEX IDX_782EAE1B4C3A6401 (request_payment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pot (id INT AUTO_INCREMENT NOT NULL, personal_id INT DEFAULT NULL, potname VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, creationdate DATETIME DEFAULT NULL, closuredate VARCHAR(255) DEFAULT NULL, balance DOUBLE PRECISION DEFAULT NULL, desactive TINYINT(1) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, location VARCHAR(255) DEFAULT NULL, INDEX IDX_1EBD730F5D430949 (personal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE requestpayment (id INT AUTO_INCREMENT NOT NULL, transaction_id INT DEFAULT NULL, datecreation DATETIME DEFAULT NULL, status VARCHAR(255) DEFAULT NULL, numbermonthlypayment INT DEFAULT NULL, interestrate DOUBLE PRECISION DEFAULT NULL, amount DOUBLE PRECISION DEFAULT NULL, accepted TINYINT(1) DEFAULT NULL, INDEX IDX_DF42A0752FC0CB0F (transaction_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transaction (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) DEFAULT NULL, transaction_type VARCHAR(255) DEFAULT NULL, status VARCHAR(255) DEFAULT NULL, category VARCHAR(255) DEFAULT NULL, amount DOUBLE PRECISION DEFAULT NULL, ttc DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transactionitem (id INT AUTO_INCREMENT NOT NULL, account_id INT DEFAULT NULL, transaction_id INT DEFAULT NULL, senderid INT DEFAULT NULL, receiverid INT DEFAULT NULL, amount DOUBLE PRECISION DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, INDEX IDX_A934892D9B6B5FBA (account_id), INDEX IDX_A934892D2FC0CB0F (transaction_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transactionpayment (id INT AUTO_INCREMENT NOT NULL, payment_method_id INT DEFAULT NULL, transaction_id INT DEFAULT NULL, paymentamount DOUBLE PRECISION DEFAULT NULL, INDEX IDX_E26611CE5AA1164F (payment_method_id), INDEX IDX_E26611CE2FC0CB0F (transaction_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE paymentmethod ADD CONSTRAINT FK_8328E04E9B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE claim ADD CONSTRAINT FK_A769DE275D430949 FOREIGN KEY (personal_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A769B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE installmentpayment ADD CONSTRAINT FK_782EAE1B4C3A6401 FOREIGN KEY (request_payment_id) REFERENCES requestpayment (id)');
        $this->addSql('ALTER TABLE pot ADD CONSTRAINT FK_1EBD730F5D430949 FOREIGN KEY (personal_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE requestpayment ADD CONSTRAINT FK_DF42A0752FC0CB0F FOREIGN KEY (transaction_id) REFERENCES transaction (id)');
        $this->addSql('ALTER TABLE transactionitem ADD CONSTRAINT FK_A934892D9B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE transactionitem ADD CONSTRAINT FK_A934892D2FC0CB0F FOREIGN KEY (transaction_id) REFERENCES transaction (id)');
        $this->addSql('ALTER TABLE transactionpayment ADD CONSTRAINT FK_E26611CE5AA1164F FOREIGN KEY (payment_method_id) REFERENCES paymentmethod (id)');
        $this->addSql('ALTER TABLE transactionpayment ADD CONSTRAINT FK_E26611CE2FC0CB0F FOREIGN KEY (transaction_id) REFERENCES transaction (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE paymentmethod DROP FOREIGN KEY FK_8328E04E9B6B5FBA');
        $this->addSql('ALTER TABLE claim DROP FOREIGN KEY FK_A769DE275D430949');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A769B6B5FBA');
        $this->addSql('ALTER TABLE pot DROP FOREIGN KEY FK_1EBD730F5D430949');
        $this->addSql('ALTER TABLE transactionitem DROP FOREIGN KEY FK_A934892D9B6B5FBA');
        $this->addSql('ALTER TABLE transactionpayment DROP FOREIGN KEY FK_E26611CE5AA1164F');
        $this->addSql('ALTER TABLE installmentpayment DROP FOREIGN KEY FK_782EAE1B4C3A6401');
        $this->addSql('ALTER TABLE requestpayment DROP FOREIGN KEY FK_DF42A0752FC0CB0F');
        $this->addSql('ALTER TABLE transactionitem DROP FOREIGN KEY FK_A934892D2FC0CB0F');
        $this->addSql('ALTER TABLE transactionpayment DROP FOREIGN KEY FK_E26611CE2FC0CB0F');
        $this->addSql('DROP TABLE account');
        $this->addSql('DROP TABLE paymentmethod');
        $this->addSql('DROP TABLE claim');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE installmentpayment');
        $this->addSql('DROP TABLE pot');
        $this->addSql('DROP TABLE requestpayment');
        $this->addSql('DROP TABLE transaction');
        $this->addSql('DROP TABLE transactionitem');
        $this->addSql('DROP TABLE transactionpayment');
    }
}
