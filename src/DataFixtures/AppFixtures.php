<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use App\Entity\VirtualCard;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    public const ADMIN_USER_REFERENCE = 'admin-user';
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var Factory
     */
    private $faker;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)

    {
        $this->passwordEncoder = $passwordEncoder;
        $this->faker = Factory::create();
    }

    /**
     * load data fixtures with te passed EntityManager
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->loadAccount($manager);

    }

    /*
        public function loadVirtualCard(ObjectManager $manager)
        {
            for ($u = 0; $u < 20; $u++) {
                $product1 = new VirtualCard();
                $product1->setCardName('card' . $u);
                $product1->setBalancedCard(mt_rand(10, 1000));
                $product1->setAccount($this->getReference(self::ADMIN_USER_REFERENCE));
                $manager->persist($product1);
            }

            $manager->flush();
        }
    */
    public function loadAccount(ObjectManager $manager)
    {

        for ($u = 0; $u < 20; $u++) {
            $account = new Admin();
            $account->setEmail($this->faker->realText(30));
            $account->setLastname($this->faker->realText(30));
            $account->setUpdatedAt(date_create(null));
            $account->setFirstname($this->faker->realText(20));
            $account->setPhonenumber(mt_rand(10, 1000));
            $account->setPassword($this->passwordEncoder->encodePassword($account, 'Mohamed'));

            $manager->persist($account);

        }

        $manager->flush();
        $this->addReference(self::ADMIN_USER_REFERENCE, $account);

    }
}
