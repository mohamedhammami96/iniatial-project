<?php

namespace App\Controller;

use App\Entity\Account;
use App\Entity\Admin;
use App\Entity\Business;
use App\Entity\Personal;
use App\Entity\Pot;
use http\Env\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/acc")
 */
class AccountsController extends AbstractController
{
    /**
     * @Route("/list",name="ListAccount",methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)

    {
        $repository = $this->getDoctrine()->getRepository(Account::class);
        $item = $repository->findAll();
        return $this->json($item);
    }

    /**
     * @Route("/add",name="AddAccountBusiness",methods={"Post"})
     * @param Request $request
     * @return JsonResponse
     */
    public function addbusiness(Request $request)
    {
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');
        $account = $serializer->deserialize($request->getContent(), Business::class, 'json');
        $em = $this->getDoctrine()->getManager();
        $em->persist($account);
        $em->flush();
        return $this->json($account);

    }

    /**
     * @Route("/add",name="AddAccountAdmin",methods={"Post"})
     * @param Request $request
     * @return JsonResponse
     */
    public function addadmin(Request $request)
    {
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');
        $account = $serializer->deserialize($request->getContent(), Admin::class, 'json');
        $em = $this->getDoctrine()->getManager();
        $em->persist($account);
        $em->flush();
        return $this->json($account);

    }

    /**
     * @Route("/add",name="AddAccountPersonal",methods={"Post"})
     * @param Request $request
     * @return JsonResponse
     */
    public function addpersonal(Request $request)
    {
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');
        $account = $serializer->deserialize($request->getContent(), Personal::class, 'json');
        $em = $this->getDoctrine()->getManager();
        $em->persist($account);
        $em->flush();
        return $this->json($account);

    }

    /**
     * @Route("/list/{id}",name="findaccountby_id",methods={"GET"})
     * @param $id
     * @return JsonResponse
     */
    public function post($id)
    {
        return $this->json($this->getDoctrine()->getRepository(Account::class)->find($id));
    }

    /**
     * @Route("/delete/{id}",name="deleteAccount",methods={"DELETE"})
     * @return JsonResponse
     */
    public function Delete($id)
    {
        $em = $this->getDoctrine()->getManager();

        $account = $em->getRepository(Account::class)->find($id);
        $em->remove($account);
        $em->flush();

        return new JsonResponse('Account deleted with succes');
    }

    /**
     * @Route("/accept/{id}",name="accept",methods={"POST"})
     * @param Account $account
     * @return JsonResponse
     */
    public function accept(Account $account)
    {

        $em = $this->getDoctrine()->getManager();
        $account->setIsactive(true);
        $em->persist($account);
        $em->flush();
        return new JsonResponse('Account Accepted');

    }

    /**
     * @Route("/reject/{id}",name="reject",methods={"POST"})
     * @param Account $account
     * @return JsonResponse
     */
    public function reject(Account $account)
    {

        $em = $this->getDoctrine()->getManager();
        $account->setIsactive(false);
        $em->persist($account);
        $em->flush();
        return new JsonResponse('Account Rejected');

    }

    /**
     * @Route("/update/{id}",name="update",methods={"PUT","POST"})
     * @return JsonResponse
     */

        public function ModifierEventAction(Account $request)
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($request);
        $em->flush();
        return $this->json($request);

    }
    }


