<?php

namespace App\Controller;
use App\Entity\Business;
use App\Entity\Pot;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/pot")
 */
class PotController extends AbstractController
{

    /**
     * @Route("/list",name="List",methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)

    {
        $repository = $this->getDoctrine()->getRepository(Pot::class);
        $item = $repository->findAll();
        return $this->json($item);
    }

    /**
     * @Route("/add",name="AddPot",methods={"Post"})
     * @param Request $request
     * @return JsonResponse
     */
    public function add(Request $request)
    {
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');
        $pot = $serializer->deserialize($request->getContent(), Pot::class, 'json');
        $em = $this->getDoctrine()->getManager();
        $em->persist($pot);
        $em->flush();
        return $this->json($pot);

    }

    /**
     * @Route("/list/{id}",name="findby_id",methods={"GET"})
     * @param $id
     * @return JsonResponse
     */
    public function post($id)
    {
        return $this->json($this->getDoctrine()->getRepository(Pot::class)->find($id));
    }

    /**
     * @Route("/delete/{id}",name="deletePot",methods={"DELETE"})
     * @return JsonResponse
     */
    public function Delete($id)
    {
        $em = $this->getDoctrine()->getManager();

        $pot = $em->getRepository(Pot::class)->find($id);
        $em->remove($pot);
        $em->flush();

        return new JsonResponse('Pot deleted with succes');
    }

    /**
     * @Route("/accept/{id}",name="accept",methods={"POST"})
     * @param Pot $pot
     * @return JsonResponse
     */
    public function accept(Pot $pot)
    {

        $em = $this->getDoctrine()->getManager();
        $pot->setDesactive(true);
        $em->persist($pot);
        $em->flush();
        return new JsonResponse('Pot Accepted');

    }

    /**
     * @Route("/reject/{id}",name="rejectpot",methods={"POST"})
     * @param Pot $pot
     * @return JsonResponse
     */
    public function reject(Pot $pot)
    {

        $em = $this->getDoctrine()->getManager();
        $pot->setDesactive(false);
        $em->persist($pot);
        $em->flush();
        return new JsonResponse('Pot Rejected');

    }

    /**
     * @Route("/update/{id}",name="update",methods={"PUT"})
     * @param Pot $request
     * @return JsonResponse
     */

    public function ModifierEventAction(Pot $request,$id)

    {
        $em = $this->getDoctrine()->getManager();
        $pot = $em->getRepository(Pot::class)->find($id);
        $em->persist($pot);
        $em->flush();
        return $this->json($pot);

    }
}


