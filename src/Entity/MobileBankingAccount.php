<?php

namespace App\Entity;

use App\Repository\MobileBankingAccountRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MobileBankingAccountRepository::class)
 */
class MobileBankingAccount extends PaymentMethod
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phonenumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $serviceprovider;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $currentbalance;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPhonenumber(): ?string
    {
        return $this->phonenumber;
    }

    public function setPhonenumber(?string $phonenumber): self
    {
        $this->phonenumber = $phonenumber;

        return $this;
    }

    public function getServiceprovider(): ?string
    {
        return $this->serviceprovider;
    }

    public function setServiceprovider(?string $serviceprovider): self
    {
        $this->serviceprovider = $serviceprovider;

        return $this;
    }

    public function getCurrentbalance(): ?float
    {
        return $this->currentbalance;
    }

    public function setCurrentbalance(?float $currentbalance): self
    {
        $this->currentbalance = $currentbalance;

        return $this;
    }
}
