<?php

namespace App\Entity;

use App\Repository\BusinessRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BusinessRepository::class)
 */
class Business extends Account
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $socialreason;


    public function getSocialreason(): ?string
    {
        return $this->socialreason;
    }

    public function setSocialreason(?string $socialreason): self
    {
        $this->socialreason = $socialreason;

        return $this;
    }
}
