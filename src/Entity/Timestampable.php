<?php
declare (strict_types=1);

namespace App\Entity;

use DateTimeInterface;

trait Timestampable
{
    /**
     * @var DateTimeInterface
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $createdAt;
    /**
     * @var DateTimeInterface
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $updatedAt;


    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt( DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }


    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }


    public function setUpdatedAt( DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

}