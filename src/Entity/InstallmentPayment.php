<?php

namespace App\Entity;

use App\Repository\InstallmentPaymentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InstallmentPaymentRepository::class)
 * @ORM\Table(name="installmentpayment")
 */
class InstallmentPayment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $refunddate;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $balance;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $ispayed;

    /**
     * @ORM\ManyToOne(targetEntity=RequestPayment::class, inversedBy="installmentPayments")
     */
    private $requestPayment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRefunddate(): ?\DateTimeInterface
    {
        return $this->refunddate;
    }

    public function setRefunddate(?\DateTimeInterface $refunddate): self
    {
        $this->refunddate = $refunddate;

        return $this;
    }

    public function getBalance(): ?float
    {
        return $this->balance;
    }

    public function setBalance(?float $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    public function getIspayed(): ?bool
    {
        return $this->ispayed;
    }

    public function setIspayed(?bool $ispayed): self
    {
        $this->ispayed = $ispayed;

        return $this;
    }

    public function getRequestPayment(): ?RequestPayment
    {
        return $this->requestPayment;
    }

    public function setRequestPayment(?RequestPayment $requestPayment): self
    {
        $this->requestPayment = $requestPayment;

        return $this;
    }
}
