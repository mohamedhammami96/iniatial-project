<?php

namespace App\Entity;

use App\Repository\RequestPaymentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RequestPaymentRepository::class)
 * @ORM\Table(name="requestpayment")
 */
class RequestPayment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $datecreation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numbermonthlypayment;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $interestrate;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $accepted;

    /**
     * @ORM\ManyToOne(targetEntity=Transaction::class, inversedBy="requestPayments")
     */
    private $transaction;

    /**
     * @ORM\OneToMany(targetEntity=InstallmentPayment::class, mappedBy="requestPayment")
     */
    private $installmentPayments;

    public function __construct()
    {
        $this->installmentPayments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatecreation(): ?\DateTimeInterface
    {
        return $this->datecreation;
    }

    public function setDatecreation(?\DateTimeInterface $datecreation): self
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getNumbermonthlypayment(): ?int
    {
        return $this->numbermonthlypayment;
    }

    public function setNumbermonthlypayment(?int $numbermonthlypayment): self
    {
        $this->numbermonthlypayment = $numbermonthlypayment;

        return $this;
    }

    public function getInterestrate(): ?float
    {
        return $this->interestrate;
    }

    public function setInterestrate(?float $interestrate): self
    {
        $this->interestrate = $interestrate;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getAccepted(): ?bool
    {
        return $this->accepted;
    }

    public function setAccepted(?bool $accepted): self
    {
        $this->accepted = $accepted;

        return $this;
    }

    public function getTransaction(): ?Transaction
    {
        return $this->transaction;
    }

    public function setTransaction(?Transaction $transaction): self
    {
        $this->transaction = $transaction;

        return $this;
    }

    /**
     * @return Collection|InstallmentPayment[]
     */
    public function getInstallmentPayments(): Collection
    {
        return $this->installmentPayments;
    }

    public function addInstallmentPayment(InstallmentPayment $installmentPayment): self
    {
        if (!$this->installmentPayments->contains($installmentPayment)) {
            $this->installmentPayments[] = $installmentPayment;
            $installmentPayment->setRequestPayment($this);
        }

        return $this;
    }

    public function removeInstallmentPayment(InstallmentPayment $installmentPayment): self
    {
        if ($this->installmentPayments->contains($installmentPayment)) {
            $this->installmentPayments->removeElement($installmentPayment);
            // set the owning side to null (unless already changed)
            if ($installmentPayment->getRequestPayment() === $this) {
                $installmentPayment->setRequestPayment(null);
            }
        }

        return $this;
    }
}
