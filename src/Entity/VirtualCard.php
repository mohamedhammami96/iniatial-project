<?php

namespace App\Entity;

use App\Repository\VirtualCardRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VirtualCardRepository::class)
 */
class VirtualCard extends  PaymentMethod
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cardnumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cardname;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expirationdate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cryptogram;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCardnumber(): ?string
    {
        return $this->cardnumber;
    }

    public function setCardnumber(?string $cardnumber): self
    {
        $this->cardnumber = $cardnumber;

        return $this;
    }

    public function getCardname(): ?string
    {
        return $this->cardname;
    }

    public function setCardname(?string $cardname): self
    {
        $this->cardname = $cardname;

        return $this;
    }

    public function getExpirationdate(): ?\DateTimeInterface
    {
        return $this->expirationdate;
    }

    public function setExpirationdate(?\DateTimeInterface $expirationdate): self
    {
        $this->expirationdate = $expirationdate;

        return $this;
    }

    public function getCryptogram(): ?int
    {
        return $this->cryptogram;
    }

    public function setCryptogram(?int $cryptogram): self
    {
        $this->cryptogram = $cryptogram;

        return $this;
    }
}
