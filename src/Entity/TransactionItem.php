<?php

namespace App\Entity;

use App\Repository\TransactionItemRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TransactionItemRepository::class)
 * @ORM\Table(name="transactionitem")
 */
class TransactionItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $senderid;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $receiverid;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Account::class, inversedBy="transactionItems")
     */
    private $account;
    /**
     * @ORM\ManyToOne(targetEntity=Transaction::class, inversedBy="transactionItems")
     */
    private $transaction;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSenderid(): ?int
    {
        return $this->senderid;
    }

    public function setSenderid(?int $senderid): self
    {
        $this->senderid = $senderid;

        return $this;
    }

    public function getReceiverid(): ?int
    {
        return $this->receiverid;
    }

    public function setReceiverid(?int $receiverid): self
    {
        $this->receiverid = $receiverid;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }
    public function getTransaction(): ?Transaction
    {
        return $this->transaction;
    }

    public function setTransaction(?Account $transaction): self
    {
        $this->transaction = $transaction;

        return $this;
    }
}
