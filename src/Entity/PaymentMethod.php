<?php

namespace App\Entity;

use App\Repository\PaymentMethodRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\InheritanceType;

/**
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="kind", type="string")
 * @ORM\DiscriminatorMap({
 * "bankingaccount"="App\Entity\BankingAccount",
 * "virtualcard"="App\Entity\VirtualCard",
 * "mobilebanking"="App\Entity\MobileBankingAccount",
 * })
 * @ORM\Table(name="paymentmethod")
 * @ORM\Entity(repositoryClass="App\Repository\PaymentMethodRepository")
 */
abstract class PaymentMethod
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Account::class, inversedBy="paymentMethods")
     */
    private $account;

    /**
     * @ORM\OneToMany(targetEntity=TransactionPayment::class, mappedBy="paymentMethod")
     */
    private $transactionPayments;

    public function __construct()
    {
        $this->transactionPayments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @return Collection|TransactionPayment[]
     */
    public function getTransactionPayments(): Collection
    {
        return $this->transactionPayments;
    }

    public function addTransactionPayment(TransactionPayment $transactionPayment): self
    {
        if (!$this->transactionPayments->contains($transactionPayment)) {
            $this->transactionPayments[] = $transactionPayment;
            $transactionPayment->setPaymentMethod($this);
        }

        return $this;
    }

    public function removeTransactionPayment(TransactionPayment $transactionPayment): self
    {
        if ($this->transactionPayments->contains($transactionPayment)) {
            $this->transactionPayments->removeElement($transactionPayment);
            // set the owning side to null (unless already changed)
            if ($transactionPayment->getPaymentMethod() === $this) {
                $transactionPayment->setPaymentMethod(null);
            }
        }

        return $this;
    }
}
