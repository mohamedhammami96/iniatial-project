<?php

namespace App\Entity;

use App\Repository\TransactionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TransactionRepository::class)
 * @ORM\Table(name="transaction")
 */
class Transaction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $transactionType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $category;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $ttc;

    /**
     * @ORM\OneToMany(targetEntity=TransactionPayment::class, mappedBy="transaction")
     */
    private $transactionPayments;
    /**
     * @ORM\OneToMany(targetEntity=TransactionItem::class, mappedBy="transaction")
     */
    private $transactionItems;

    /**
     * @ORM\OneToMany(targetEntity=RequestPayment::class, mappedBy="transaction")
     */
    private $requestPayments;

    public function __construct()
    {
        $this->transactionPayments = new ArrayCollection();
        $this->transactionItems = new  ArrayCollection();
        $this->requestPayments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getTransactionType(): ?string
    {
        return $this->transactionType;
    }

    public function setTransactionType(?string $transactionType): self
    {
        $this->transactionType = $transactionType;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getTtc(): ?float
    {
        return $this->ttc;
    }

    public function setTtc(?float $ttc): self
    {
        $this->ttc = $ttc;

        return $this;
    }

    /**
     * @return Collection|TransactionPayment[]
     */
    public function getTransactionPayments(): Collection
    {
        return $this->transactionPayments;
    }

    public function addTransactionPayment(TransactionPayment $transactionPayment): self
    {
        if (!$this->transactionPayments->contains($transactionPayment)) {
            $this->transactionPayments[] = $transactionPayment;
            $transactionPayment->setTransacton($this);
        }

        return $this;
    }

    public function removeTransactionPayment(TransactionPayment $transactionPayment): self
    {
        if ($this->transactionPayments->contains($transactionPayment)) {
            $this->transactionPayments->removeElement($transactionPayment);
            // set the owning side to null (unless already changed)
            if ($transactionPayment->getTransacton() === $this) {
                $transactionPayment->setTransacton(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TransactionItem[]
     */
    public function getTransactionItems(): Collection
    {
        return $this->transactionItems;
    }

    public function addTransactionItem(TransactionItem $transactionItem): self
    {
        if (!$this->transactionItems->contains($transactionItem)) {
            $this->transactionItems[] = $transactionItem;
            $transactionItem->setTransacton($this);
        }

        return $this;
    }

    public function removeTransactionItem(TransactionItem $transactionItem): self
    {
        if ($this->transactionItems->contains($transactionItem)) {
            $this->transactionItems->removeElement($transactionItem);
            // set the owning side to null (unless already changed)
            if ($transactionItem->getTransacton() === $this) {
                $transactionItem->setTransacton(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RequestPayment[]
     */
    public function getRequestPayments(): Collection
    {
        return $this->requestPayments;
    }

    public function addRequestPayment(RequestPayment $requestPayment): self
    {
        if (!$this->requestPayments->contains($requestPayment)) {
            $this->requestPayments[] = $requestPayment;
            $requestPayment->setTransaction($this);
        }

        return $this;
    }

    public function removeRequestPayment(RequestPayment $requestPayment): self
    {
        if ($this->requestPayments->contains($requestPayment)) {
            $this->requestPayments->removeElement($requestPayment);
            // set the owning side to null (unless already changed)
            if ($requestPayment->getTransaction() === $this) {
                $requestPayment->setTransaction(null);
            }
        }

        return $this;
    }
}
