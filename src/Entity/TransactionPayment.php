<?php

namespace App\Entity;

use App\Repository\TransactionPaymentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TransactionPaymentRepository::class)
 * @ORM\Table(name="transactionpayment")
 */
class TransactionPayment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $paymentamount;

    /**
     * @ORM\ManyToOne(targetEntity=PaymentMethod::class, inversedBy="transactionPayments")
     */
    private $paymentMethod;

    /**
     * @ORM\ManyToOne(targetEntity=Transaction::class, inversedBy="transactionPayments")
     */
    private $transaction;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPaymentamount(): ?float
    {
        return $this->paymentamount;
    }

    public function setPaymentamount(?float $paymentamount): self
    {
        $this->paymentamount = $paymentamount;

        return $this;
    }

    public function getPaymentMethod(): ?PaymentMethod
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?PaymentMethod $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getTransaction(): ?Transaction
    {
        return $this->transaction;
    }

    public function setTransaction(?Transaction $transaction): self
    {
        $this->transaction = $transaction;

        return $this;
    }
}
