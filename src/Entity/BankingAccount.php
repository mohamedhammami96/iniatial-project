<?php

namespace App\Entity;

use App\Repository\BankingAccountRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BankingAccountRepository::class)
 */
class BankingAccount extends  PaymentMethod
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bankname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $accounttype;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBankname(): ?string
    {
        return $this->bankname;
    }

    public function setBankname(?string $bankname): self
    {
        $this->bankname = $bankname;

        return $this;
    }

    public function getAccounttype(): ?string
    {
        return $this->accounttype;
    }

    public function setAccounttype(?string $accounttype): self
    {
        $this->accounttype = $accounttype;

        return $this;
    }
}
