<?php

namespace App\Entity;

use App\Repository\PersonalRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PersonalRepository::class)
 */
class Personal extends Account
{


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $score;

    /**
     * @ORM\OneToMany(targetEntity=Claim::class, mappedBy="personal")
     */
    private $claims;

    /**
     * @ORM\OneToMany(targetEntity=Pot::class, mappedBy="personal")
     */
    private $pots;

    public function __construct()
    {
        parent::__construct();
        $this->claims = new ArrayCollection();
        $this->pots = new ArrayCollection();
    }


    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(?int $score): self
    {
        $this->score = $score;

        return $this;
    }

    /**
     * @return Collection|Claim[]
     */
    public function getClaims(): Collection
    {
        return $this->claims;
    }

    public function addClaim(Claim $claim): self
    {
        if (!$this->claims->contains($claim)) {
            $this->claims[] = $claim;
            $claim->setPersonal($this);
        }

        return $this;
    }

    public function removeClaim(Claim $claim): self
    {
        if ($this->claims->contains($claim)) {
            $this->claims->removeElement($claim);
            // set the owning side to null (unless already changed)
            if ($claim->getPersonal() === $this) {
                $claim->setPersonal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Pot[]
     */
    public function getPots(): Collection
    {
        return $this->pots;
    }

    public function addPot(Pot $pot): self
    {
        if (!$this->pots->contains($pot)) {
            $this->pots[] = $pot;
            $pot->setPersonal($this);
        }

        return $this;
    }

    public function removePot(Pot $pot): self
    {
        if ($this->pots->contains($pot)) {
            $this->pots->removeElement($pot);
            // set the owning side to null (unless already changed)
            if ($pot->getPersonal() === $this) {
                $pot->setPersonal(null);
            }
        }

        return $this;
    }
}
