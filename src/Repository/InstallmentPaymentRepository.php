<?php

namespace App\Repository;

use App\Entity\InstallmentPayment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InstallmentPayment|null find($id, $lockMode = null, $lockVersion = null)
 * @method InstallmentPayment|null findOneBy(array $criteria, array $orderBy = null)
 * @method InstallmentPayment[]    findAll()
 * @method InstallmentPayment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstallmentPaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InstallmentPayment::class);
    }

    // /**
    //  * @return InstallmentPayment[] Returns an array of InstallmentPayment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InstallmentPayment
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
