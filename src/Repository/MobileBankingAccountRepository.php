<?php

namespace App\Repository;

use App\Entity\MobileBankingAccount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MobileBankingAccount|null find($id, $lockMode = null, $lockVersion = null)
 * @method MobileBankingAccount|null findOneBy(array $criteria, array $orderBy = null)
 * @method MobileBankingAccount[]    findAll()
 * @method MobileBankingAccount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MobileBankingAccountRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MobileBankingAccount::class);
    }

    // /**
    //  * @return MobileBankingAccount[] Returns an array of MobileBankingAccount objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MobileBankingAccount
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
