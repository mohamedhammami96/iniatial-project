<?php

namespace App\Repository;

use App\Entity\BankingAccount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BankingAccount|null find($id, $lockMode = null, $lockVersion = null)
 * @method BankingAccount|null findOneBy(array $criteria, array $orderBy = null)
 * @method BankingAccount[]    findAll()
 * @method BankingAccount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BankingAccountRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BankingAccount::class);
    }

    // /**
    //  * @return BankingAccount[] Returns an array of BankingAccount objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BankingAccount
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
