<?php

namespace App\Repository;

use App\Entity\TransactionItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TransactionItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method TransactionItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method TransactionItem[]    findAll()
 * @method TransactionItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TransactionItem::class);
    }

    // /**
    //  * @return TransactionItem[] Returns an array of TransactionItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TransactionItem
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
