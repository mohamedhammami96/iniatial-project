<?php

namespace App\Repository;

use App\Entity\RequestPayment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RequestPayment|null find($id, $lockMode = null, $lockVersion = null)
 * @method RequestPayment|null findOneBy(array $criteria, array $orderBy = null)
 * @method RequestPayment[]    findAll()
 * @method RequestPayment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RequestPaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RequestPayment::class);
    }

    // /**
    //  * @return RequestPayment[] Returns an array of RequestPayment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RequestPayment
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
