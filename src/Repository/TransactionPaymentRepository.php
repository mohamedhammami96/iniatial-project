<?php

namespace App\Repository;

use App\Entity\TransactionPayment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TransactionPayment|null find($id, $lockMode = null, $lockVersion = null)
 * @method TransactionPayment|null findOneBy(array $criteria, array $orderBy = null)
 * @method TransactionPayment[]    findAll()
 * @method TransactionPayment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionPaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TransactionPayment::class);
    }

    // /**
    //  * @return TransactionPayment[] Returns an array of TransactionPayment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TransactionPayment
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
